import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { ListaInscritosComponent } from './components/lista-inscritos/lista-inscritos.component';
import { ListaPuntosComponent } from './components/lista-puntos/lista-puntos.component';
import { AcreditacionComponent } from './components/acreditacion/acreditacion.component';
import { GrupoComponent } from './components/grupo/grupo.component';
import { CanjePuntosComponent } from './components/canje-puntos/canje-puntos.component';
import { AcreditarGrupoComponent} from './components/acreditar-grupo/acreditar-grupo.component';
import { InscripcionComponent } from './components/inscripcion/inscripcion.component';
import { InfoComponent } from './components/info/info.component';
import { AuthGuard } from './guards/auth.guard';

const appRoutes: Routes = [
    { path: '', redirectTo: 'puntos', pathMatch: 'full' },
    { path: 'puntos', component: ListaPuntosComponent, data: { state: 'puntos' }},
    { path: 'mi-grupo', component: GrupoComponent, canActivate: [AuthGuard], data: { state: 'mi-grupo' } },
    { path: 'canje-puntos', component: CanjePuntosComponent, canActivate: [AuthGuard], data: { state: 'canje-puntos' }},
    { path: 'inscripcion', component: InscripcionComponent, canActivate: [AuthGuard], data: { state: 'inscripcion' }},
    { path: 'acreditar', component: ListaInscritosComponent, canActivate: [AuthGuard], data: { state: 'acreditar' }},
    { path: 'acreditar-inscrito', component: AcreditacionComponent, canActivate: [AuthGuard] },
    { path: 'acreditar-grupo', component: AcreditarGrupoComponent, canActivate: [AuthGuard], data: { state: 'acreditar-grupo' } },
    { path: 'info', component: InfoComponent, canActivate: [AuthGuard], data: { state: 'info' } }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}




