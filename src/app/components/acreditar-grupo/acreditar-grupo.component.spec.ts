import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcreditarGrupoComponent } from './acreditar-grupo.component';

describe('AcreditarGrupoComponent', () => {
  let component: AcreditarGrupoComponent;
  let fixture: ComponentFixture<AcreditarGrupoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcreditarGrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcreditarGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
