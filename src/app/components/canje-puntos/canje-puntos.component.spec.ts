import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanjePuntosComponent } from './canje-puntos.component';

describe('CanjePuntosComponent', () => {
  let component: CanjePuntosComponent;
  let fixture: ComponentFixture<CanjePuntosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanjePuntosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanjePuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
