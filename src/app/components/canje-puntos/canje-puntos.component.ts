import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { FireService } from './../../services/fire.service';



@Component({
  selector: 'app-canje-puntos',
  templateUrl: './canje-puntos.component.html',
  styleUrls: ['./canje-puntos.component.css']
})
export class CanjePuntosComponent implements OnInit {
  acreditado;
  proceder = false;
  filtro = true;
  acreditados: any[];
  premiosSelecionados = [];
  cantidadPremios = [];
  premios = [];
  total = 0;
  puedeCanjear = false;
  acreditadoSeleccionado = false;
  searchText;
  constructor(private fire: FireService, private db: AngularFireDatabase) {
    // this.item = db.object('guias/29').valueChanges();
    // this.item.subscribe( data => {
    //   console.log(data);
    // });
    this.fire.getPremios().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(premios => {
        for (let i = 0; i < premios.length; i++) {
          if (!(premios[i].cantidad_disponible > 0)) {
            premios.splice(i, 1);
          }
          this.premios = premios;
        }
      });
  }

  ngOnInit() {
    this.fire.getAcreditados(true).snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(acreditados => {
        this.acreditados = acreditados;
        // console.log('se actualizo lista');
      });
  }
  cambiarFiltro() {
    this.filtro = !this.filtro;
  }
  seleccionar($event, acreditado) {
    $event.preventDefault();
    this.acreditado = acreditado;
    this.acreditadoSeleccionado = true;
    this.proceder = false;
    // console.log(acreditado);
  }
  buscar() {
    this.proceder = true;
    this.acreditadoSeleccionado = false;
  }
  agregar(premio) {
    if (this.premiosSelecionados.includes(premio)) {
      for (let i = 0; i < this.cantidadPremios.length; i++) {
        if (premio.key === this.cantidadPremios[i].key) {
          this.cantidadPremios[i].cantidad += 1;
        }
        // console.log(this.cantidadPremios[i]);
      }
    }else {
      this.premiosSelecionados.push(premio);
      const p = premio;
      p.cantidad = 1;
      this.cantidadPremios.push(p);
    }
    this.total = this.total + premio.precio;
  }
  cancelar() {
    this.proceder = false;
    this.acreditadoSeleccionado = false;
    this.premiosSelecionados.length = 0;
    this.cantidadPremios.length = 0;
    this.total = 0;
  }
  canjear() {
    for (let i = 0; i < this.cantidadPremios.length; i++) {
      this.fire.canjearPuntos(this.cantidadPremios[i]);
    }
    const valor = this.acreditado.puntos - this.total;
    this.fire.restarpuntos(this.acreditado, valor);
    this.cancelar();
  }
   // buscar(input) {
  //   this.db.database.ref(`acreditado/${input}`)
  //     .once('value').then((snap) => {
  //       if (snap.exists()) {
  //         this.acreditado = snap.val();
  //         if (this.acreditado.puntos > 0) {
  //           this.proceder = true;
  //         } else {
  //           console.log('no puede canjear');
  //         }
  //       }else {
  //         console.log('NO existe usuario');
  //       }
  //     });
  // }
}

