import { Component, OnInit, OnDestroy} from '@angular/core';
import { FireService } from '../../services/fire.service';
import { DatosService } from './../../services/datos.service';
import { AuthService } from './../../services/auth.service';
import { Guia } from '../../models/guia';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operator/debounceTime';
@Component({
  selector: 'app-grupo',
  templateUrl: './grupo.component.html',
  styleUrls: ['./grupo.component.css']
})
export class GrupoComponent implements OnInit, OnDestroy {
  acreditados: any[];
  existeTicket: any[];
  message;
  lista;
  usuario;
  adTicket = false;
  seleccionado = false;
  acreditadoSeleccionado;
  numeroTicket: number;

  //alerta
  private _success = new Subject<string>();
  successMessage: string;
  private _success2 = new Subject<string>();
  successMessage2: string;

  constructor(private fire: FireService, private data: DatosService, private auth: AuthService) {
    // obtenemos los usuarios
    this.fire.getUsuario(this.auth.getUsuario().id).snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(snap => {
        // console.log(snap[0]);
        // datos del usuario
        this.usuario = snap[0];
      });


    this.data.currentMessage2.subscribe((message) => {
      this.message = message;
      if (this.message != null) {
        this.lista = this.fire.getGrupo(this.message.id).snapshotChanges()
        .map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        this.lista.subscribe(acreditados => this.acreditados = acreditados);
        }
    });

    //alerta

    this._success.subscribe((message) => this.successMessage = message);
    debounceTime.call(this._success, 5000).subscribe(() => this.successMessage = null);


    this._success2.subscribe((message) => this.successMessage2 = message);
    debounceTime.call(this._success2, 5000).subscribe(() => this.successMessage2 = null);
   }
  agregarTicket(inscrito) {
    this.fire.agregarTicket(inscrito, this.usuario);
    this.changeSuccessMessage2('PUNTO AGREGADO...');
  }
  ngOnInit() {
  }
  adicionarTicket () {
    this.adTicket = true;
  }
  seleccionar($event, acreditado) {
    $event.preventDefault();
    this.acreditadoSeleccionado = acreditado;
    this.seleccionado = true;
  }
  registrarTicket() {
    if (this.seleccionado) {
      if (this.numeroTicket != null && this.numeroTicket !== 0) {
        this.fire.verificaTicket(this.numeroTicket).once('value').then((snap) => {
          if (!snap.exists()) {
            this.fire.agregarTicket2(this.usuario, this.acreditadoSeleccionado, this.numeroTicket);
            this.acreditadoSeleccionado = null;
            this.changeSuccessMessage('OK! Ticket registrado...');
          }else {
            this.changeSuccessMessage('NUM. TICKET INVALIDO... YA ESTA REGISTRADO...');
            this.numeroTicket = null;
          }
        });
      } else {
        this.changeSuccessMessage('NUM. TICKET INVALIDO');
        this.acreditadoSeleccionado = null;
      }
    }else {
      // alert('Nro. ticket invalido o sin acreditado seleccionado');
      this.changeSuccessMessage('Debe selecionar una persona...');
    }
    this.adTicket = false;
    this.seleccionado = false;
  }
  getNumeroTicket($event) {
    this.numeroTicket = $event.target.value;
  }
  // alerta
  public changeSuccessMessage(mensaje) {
    this._success.next(mensaje);
  }
  public changeSuccessMessage2(mensaje) {
    this._success2.next(mensaje);
  }
  ngOnDestroy() {
  }
}
