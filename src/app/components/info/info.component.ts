import { Component, OnInit } from '@angular/core';
import { FireService } from '../../services/fire.service';


@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  inscritos: number;
  acreditados: number;
  numMujeres;
  numHombres;
  numTickets;
  acreditado;
  guias;
  constructor(private fire: FireService) {
    this.fire.getInscritos().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(inscritos => this.inscritos = inscritos.length);

    this.fire.getAcreditados2(0).snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(acreditados => this.acreditados = acreditados.length);

    this.fire.getAcreditados2(1).snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(numMujeres => {
        this.numMujeres = numMujeres.length;
      });
    this.fire.getAcreditados2(2).snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(numHombes => {
        this.numHombres = numHombes.length;
      });

    this.fire.getNumTickets().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(numTickets => this.numTickets = numTickets.length);
    this.fire.getAcretidatoPuntos().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(snap => this.acreditado = snap[0]);

    this.fire.getNumTicketsGuias().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(guias => this.guias = guias);
  }

  ngOnInit() {
  }

}
