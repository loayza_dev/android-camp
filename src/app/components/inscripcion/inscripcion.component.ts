import { Component, OnInit } from '@angular/core';
import { Inscrito } from '../../models/inscrito';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operator/debounceTime';



@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  styleUrls: ['./inscripcion.component.css']
})
export class InscripcionComponent implements OnInit {

  private _success = new Subject<string>();
  staticAlertClosed = false;
  successMessage: string;


  nombre;
  correo;
  ci;
  genero= 'Femenino';
  id: number;
  inscrito: Inscrito;

  constructor(private db: AngularFireDatabase) {
    this.inscrito = new Inscrito();
   }

  ngOnInit() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    debounceTime.call(this._success, 15000).subscribe(() => this.successMessage = null);
  }

  public changeSuccessMessage(mensaje) {
    this._success.next(mensaje);
  }
  inscribir() {
    if (this. nombre && this.correo && this.ci ) {
      this.inscrito.nombre = this.nombre;
      this.inscrito.correo = this.correo;
      this.inscrito.ci = Number(this.ci);
      this.inscrito.genero = this.genero;


      this.db.database.ref('inscritos').orderByKey().limitToLast(1)
        .once('value').then((snap) => {
          const ins = snap.val()[Object.keys(snap.val())[0]];
          this.inscrito.id = ins.id + 1;
          this.db.list('inscritos').set(String(this.inscrito.id), this.inscrito).then(() =>
            this.changeSuccessMessage('PERSONA INSCRITA CON EL CODIGO: ' + this.inscrito.id)
          );
          console.log(ins);
        });
      console.log(this.inscrito);
      this.nombre = '';
      this.correo = '';
      this.ci = '';
      this.genero = 'Femenino';
    }else {
      this.changeSuccessMessage('TODOS LOS CAMPOS DEBEN ESTAR LLENOS');
    }
  }
  cambiarValor($event) {
    this.genero = $event.target.value;
  }
}
