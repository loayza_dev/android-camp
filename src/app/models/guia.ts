export class Guia {
    id: number;
    nombre: string;
    apellido: string;
    correo: string;
    num_participantes: number;
    num_tickets;
    rol;
    constructor() { }
}
