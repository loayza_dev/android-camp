import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string, filtro: boolean): any[] {
    if (!items) {
       return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLowerCase();
    if (!filtro) {
      return items.filter(it => {
        return it['nombre'].toLowerCase().includes(searchText);
      });
    }else {
      return items.filter(it => {
        return String(it['id']).toLowerCase().includes(searchText);
      });
    }
  }
}
