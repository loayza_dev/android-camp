import { trigger, animate, style, group, query, transition } from '@angular/animations';


export const routerTransition = trigger('routerTransition', [
    transition('* => puntos', [
        query(':enter, :leave',
        style({ position: 'fixed', width: '100%' }),
        { optional: true }),
        group([
            query(':enter', [
                style({ transform: 'translateX(-100%)' }),
                animate('0.5s ease-in-out',
                style({ transform: 'translateX(0%)' }))
                ],
            { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                style({ transform: 'translateX(100%)' }))
            ],
            { optional: true })
        ])
    ]),

    transition('* => canje-puntos', [
        group([
            query(':enter, :leave',
                style({ position: 'fixed', width: '100%' }),
                { optional: true }),

            query(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('0.5s ease-in-out',
                style({ transform: 'translateX(0%)' }))
            ],
            { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                style({ transform: 'translateX(-100%)' }))
            ],
            { optional: true })
        ])
    ]),

    transition('* => mi-grupo', [
        group([
            query(':enter, :leave',
                style({ position: 'fixed', width: '100%' }),
                { optional: true }),

            query(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(0%)' }))
            ],
                { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(-100%)' }))
            ],
                { optional: true })
        ])
    ]),

    transition('* => inscripcion', [
        group([
            query(':enter, :leave',
                style({ position: 'fixed', width: '100%' }),
                { optional: true }),

            query(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(0%)' }))
            ],
                { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(-100%)' }))
            ],
                { optional: true })
        ])
    ]),

    transition('* => acreditar', [
        group([
            query(':enter, :leave',
                style({ position: 'fixed', width: '100%' }),
                { optional: true }),

            query(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(0%)' }))
            ],
                { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(-100%)' }))
            ],
                { optional: true })
        ])
    ]),

    transition('* => acreditar-grupo', [
        group([
            query(':enter, :leave',
                style({ position: 'fixed', width: '100%' }),
                { optional: true }),

            query(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(0%)' }))
            ],
                { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(-100%)' }))
            ],
                { optional: true })
        ])
    ]),

    transition('* => info', [
        group([
            query(':enter, :leave',
                style({ position: 'fixed', width: '100%' }),
                { optional: true }),

            query(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(0%)' }))
            ],
                { optional: true }),

            query(':leave', [
                style({ transform: 'translateX(0%)' }),
                animate('0.5s ease-in-out',
                    style({ transform: 'translateX(-100%)' }))
            ],
                { optional: true })
        ])
    ])
]);

