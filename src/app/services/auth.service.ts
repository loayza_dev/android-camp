import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
// import { FireService } from './../services/fire.service';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { DatosService } from './../services/datos.service';
import { Guia } from './../models/guia';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {

  authState: Observable<firebase.User>;
  usuario;
  isLoggedIn;
  private subject = new Subject<any>();
  isAdmin = false;
  tieneGrupo;
  guia: Guia;
  private mensaje = new BehaviorSubject<Guia>(this.guia);
  currentMessage2 = this.mensaje.asObservable();

  constructor(private data: DatosService, public afAuth: AngularFireAuth,
    private db: AngularFireDatabase) {
    this.authState = afAuth.authState;
    this.authState.subscribe(
      (auth) => {
        if (auth != null) {
          this.db.database.ref('guias').orderByChild('correo').equalTo(String(auth.email))
            .once('value').then((snap) => {
              if (snap.exists()) {
                // let guia = new Guia();
                const guia = snap.val()[Object.keys(snap.val())[0]];
                this.data.changeGuia(guia);
                this.changeGuia(guia);
                // console.log(guia.rol);
                if (guia.rol === 1) {
                  this.isAdmin = true;
                  // pruebas
                  this.subject.next({ text: this.isAdmin });
                  // console.log(guia.rol);
                } else {
                  this.isAdmin = false;
                  // pruebas
                  this.subject.next({ text: this.isAdmin });
                  // console.log('no es admin');
                }
                // console.log(guia.rol);
                this.isLoggedIn = true;
                // obtener el usuario
                this.db.object(`guias/${guia.id}`).valueChanges().subscribe(datos => {
                  this.usuario = datos;
                });
                this.usuario = this.db.object(`guias/${guia.id}`).valueChanges();
              } else {
                console.log('NO existe usuario');
                this.isLoggedIn = false;
                this.logout();
              }
            });
        }else {
          this.isLoggedIn = false;
        }
      });
   }
  login() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .catch (function (error) {
      console.log(error.message);
    });
  }
  logout() {
    this.afAuth.auth.signOut();
    localStorage.clear();
    console.log('cuenta cerrada...');
  }
  isAuthenticated() {
    return this.isLoggedIn;
  }
  getUsuario() {
    return this.usuario;
  }
  changeGuia(message) {
    this.mensaje.next(message);
  }
  // prueba
  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
