import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Inscrito } from './../models/inscrito';
import { Guia } from './../models/guia';

@Injectable()
export class DatosService {
  inc: Inscrito = new Inscrito();
  guia: Guia = new Guia();
  private messageSource = new BehaviorSubject<Inscrito>(this.inc);
  currentMessage = this.messageSource.asObservable();
  private messageSource2 = new BehaviorSubject<Guia>(this.guia);
  currentMessage2 = this.messageSource2.asObservable();
  constructor() { }
  changeMessage(message: Inscrito) {
    this.messageSource.next(message);
  }
  changeGuia(message: Guia) {
    this.messageSource2.next(message);
  }
}
